<?php
include "comment.php";
include "../utils.php";

//echo $_POST["access_token"];

$user_id = get_userid($_POST["access_token"]);
if($user_id!==0) {
	$comment = new Comment($user_id, intval($_POST["item_id"]), $_POST["comment"]);
	$comment_id = $comment->save_comment();
	RestUtils::sendResponse(200, json_encode(array('comment'=>$comment->get_comment_array($comment_id))), 'application/json');
} else {
	RestUtils::sendResponse(400, "Invalid Access Token");
}
