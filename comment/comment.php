<?php
include_once "../stdlib.php";
include_once "../database_connection.php";

class Comment {
	private $id;
	private $user_id;
	private $item_id;
	private $comment_text;

	public function __construct($user_id = "", $item_id = 0, $comment_text = "") {
		$this->user_id = $user_id;
		$this->item_id = $item_id;
		$this->comment_text = $comment_text;
	}

	public function save_comment() {
		$dbconn = connect();

		$sql = "INSERT into comment(user_id, item_id, comment_text) 
				 VALUES('$this->user_id', $this->item_id, '$this->comment_text')";
		$error = $dbconn->query($sql);
		$this->id = $dbconn->insert_id;
		$dbconn->close();
		return $this->id;
	}

	public function get_comment($id) {
		$dbconn = connect();
		$sql = "SELECT c.*, m.value as vote_value from comment c
				 LEFT JOIN vote v on c.item_id = v.item_id and c.user_id = v.user_id
				 LEFT JOIN master_vote_value m ON m.id = v.vote_value_id
				 WHERE c.id = $id";
		//$sql = "select * from comment where id =$id";
		//echo $sql;
		$result = $dbconn->query($sql);
		$dbconn->close();
		return $result;
	}

	public function get_comment_from_item($item_id) {
		$dbconn = connect();
		$sql = "SELECT c . * , m.value AS vote_value
				FROM comment c
				LEFT JOIN vote v ON c.item_id = v.item_id
				AND c.user_id = v.user_id
				LEFT JOIN master_vote_value m ON m.id = v.vote_value_id
				WHERE c.item_id = $item_id";
		$result = $dbconn->query($sql);
		//echo $sql;
		$dbconn->close();
		return $result;
	}

	public function get_comment_array($id) {
		$result = $this->get_comment($id);
		$comment_array = $result->fetch_assoc();
		return $comment_array;
	}

	public function get_comment_for_user_item($user_id, $item_id) {
		$dbconn = connect();
		$sql = "";
		$result = $dbconn->query($sql);
		$dbconn->close();
	}
}
