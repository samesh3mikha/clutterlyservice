<?php
include_once "../stdlib.php";
include_once "../database_connection.php";

class Vote {
	public $id;
	public $user_id;
	public $item_id;
	public $vote_value;
	public $master_vote_id;

	public function __construct($user_id = "", $item_id = 0, $vote_value = "") {
		$this->user_id = $user_id;
		$this->item_id = $item_id;
		$this->vote_value = $vote_value;
	}

	public function save_vote() {
		$row = $this->get_vote_by_item_user($this->item_id, $this->user_id);
		//echo "row: " . $row["id"];
		if ($row["id"]==null) {
			$dbconn = connect();
			$sql = "SELECT id from master_vote_value where upper('$this->vote_value') like upper(value)";
			$result = $dbconn->query($sql);
			$row = mysqli_fetch_assoc($result);
			if ($row != null) {
				$this->master_vote_id = intval($row['id']);
				$sql = "INSERT into vote(user_id, item_id, vote_value_id) 
					 values('$this->user_id', $this->item_id, $this->master_vote_id)";
				$error = $dbconn->query($sql);
				$this->id = $dbconn->insert_id;
				return $this->id;
			} else {
				return "Not a Valid Vote";
			}
		} else {
			return "Already voted.";
		}
	}

	public function get_vote_by_item_user($item_id, $user_id) {
		$dbconn = connect();
		$sql = "select * from vote where item_id=$item_id and user_id='$user_id'";
		//echo $sql;
		$result = $dbconn->query($sql);
		$row = $result->fetch_assoc();
		return $row;
	}

	public function get_vote($id) {
		$dbconn = connect();
		$sql = "SELECT v.id id, v.user_id, v.item_id, m.value value from vote v
				join master_vote_value m on v.vote_value_id = m.id
				 WHERE v.id = $id";
		$result = $dbconn->query($sql);
		$dbconn->close();
		return $result;
	}

	public function get_vote_from_item($item_id) {
		$dbconn = connect();
		$sql = "SELECT v.id id, v.user_id, v.item_id, m.value value from vote v
				join master_vote_value m on v.vote_value_id = m.id
				 WHERE v.item_id = $item_id";
		$result = $dbconn->query($sql);
		$dbconn->close();
		return $result;
	}

	public function get_vote_array($id) {
		$result = $this->get_vote($id);
		$vote_array = $result->fetch_assoc();
		return $vote_array;
	}

	public function get_vote_counts($item_id) {
		$dbconn = connect();
		//$sql = "SELECT * FROM vote";
		$sql = "select m.value, count(iv.id) from master_vote_value m 
				left join (select id, vote_value_id from vote where item_id=$item_id)  iv
				on iv.vote_value_id = m.id
				group by m.id";
		//echo $sql;
		$result = $dbconn->query($sql);
		$dbconn->close();
		$votes = array();
		if (mysqli_num_rows($result)) {
			while ($vote = $result->fetch_array()) {
				//$votes[] = array($vote[0]=>$vote[1]);
				$votes[$vote[0]] = $vote[1];
			}
			return $votes;
		}
	}
}
