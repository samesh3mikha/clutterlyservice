<?php
include "vote.php";
include "../utils.php";

$user_id = get_userid($_POST["access_token"]);
if($user_id!==0) {
	$vote = new Vote($user_id, intval($_POST["item_id"]), $_POST["vote"]);
	$vote_id = $vote->save_vote();
	if(is_int($vote_id)) {
		//recently inserted vote and count of all votes
		$votes['vote'] = $vote->get_vote_array($vote_id);
		$votes['results']=$vote->get_vote_counts($_POST["item_id"]);
		RestUtils::sendResponse(200, json_encode($votes), 'application/json');
	} else {
		RestUtils::sendResponse(400, json_encode(array("error" => $vote_id)));
	}
} else {
	RestUtils::sendResponse(400, "Invalid Access Token");
}