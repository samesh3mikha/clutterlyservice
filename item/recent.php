<?php
include "item.php";
include "../classes/RestUtils.php";

$item = new Item();

$last_item_id = isset($_GET["last_item_id"]) ? intval($_GET["last_item_id"]) : 0;
$last_created_at = isset($_GET["last_created_at"]) ? $_GET["last_created_at"] : "";
//echo "pdp: " . $last_created_at;
$result = $_GET['category_id']!=null ? 
			$item->getRecentItemsWithCategory($_GET['num'], intval($_GET['category_id']), $last_item_id, $last_created_at) :
			$item->getRecentItems($_GET['num'], $last_item_id, $last_created_at);

$created_at = "";
$items = array();
if(mysqli_num_rows($result)) {
	while($itemRow = $result->fetch_assoc()) {
		$created_at = $itemRow["created_at"];
		$items[] = array('item'=>$itemRow);
	}
}

$remaining_items = $item->getRemainingItemCount($created_at, intval($_GET['category_id']));

$returnVal["items"] = $items;
$returnVal["remaining_items"] = $remaining_items;

RestUtils::sendResponse(200, json_encode($returnVal), 'application/json');

?>