<?php
include_once 'item.php';
include_once '../comment/comment.php';
include_once '../vote/vote.php';

$item_id = intval($_GET['item_id']);
//did not write a full join query because would have had to iterate to format the results anyway
$item = new Item();
//$item_result = $item->getItemArray($item_id);
$full_data['item'] = $item->getItemArray($item_id);

$vote = new Vote();
//vote count data
$full_data['results'] = $vote->get_vote_counts($item_id);
//vote data
$vote_result = $vote->get_vote_from_item($item_id);
if(mysqli_num_rows($vote_result)) {
	while($vote = $vote_result->fetch_assoc()) {
		//echo "odsfakjdsoj";
		$votes[] = array('vote'=>$vote);
	}
}
$full_data['votes'] = $votes;

$comment = new Comment();
$comment_result = $comment->get_comment_from_item($item_id);
//had to format here since more than one row is given as a result
if(mysqli_num_rows($comment_result)) {
	while($comment = $comment_result->fetch_assoc()) {
		//echo "odsfakjdsoj";
		$comments[] = array('comment'=>$comment);
	}
}
$full_data['comments'] = $comments;

echo json_encode($full_data);


