<?php
include_once "../stdlib.php";
include_once "../database_connection.php";

class Item {
	private $id;
	private $title;
	private $description;
	private $used;
	private $category_id;
	private $created_at;
	private $updated_at;
	private $user_id = "";
	private $graph_url = "https://graph.facebook.com/me?access_token=";
	private $image_url_original = "";
	private $image_url_thumb;
	private $folder_name = "../images/";

	public function __construct($title = "", $description = "", $used = 0,
			$category_id = 0, $access_token = "") {
		$this->title = $title;
		$this->description = $description;
		$this->used = $used;
		$this->category_id = $category_id;
		if ($access_token !== "") {
			$url = $this->graph_url . $access_token;
			$headers = get_headers($url);
			if (substr($headers[0], 9, 3) == '200') {
				$user = json_decode(file_get_contents($url));
				$this->user_id = $user->id;
			}
		}
	}

	public function saveImage($image) {
		//error_log("inside save", 0);
		if ($this->user_id == "") {
			return "invalid access token";
		}
		$error = "";
		//$image = $_FILES??
		//error_log("inside save", 0);
		//error_log("image type:  " . $image["userfile"]["type"], 0);
		if ($image["userfile"]["error"] > 0) {
			$error = "Return Code: " . $image["userfile"]["error"];
			//echo "Return Code: " . $image["userfile"]["error"] . "<br />";
			error_log($error, 0);
			switch ($_FILES['userfile']['error']) {
			case 1:
				$error = 'File exceeded upload_max_filesize';
				break;
			case 2:
				$error = 'File exceeded max_filesize';
				break;
			case 3:
				$error = 'File only partially uploaded';
				break;
			case 4:
				$error = 'No file uploaded';
				break;
			}

			error_log("error msg: $error");
			return $error;

		}
		$filename = microtime() . "_" . basename($image['userfile']['name']);
		$target_path = $this->folder_name . $filename;
		//echo "<br/>type". $image["userfile"]["type"];
		//echo "<br/>size". $image["userfile"]["size"];
		if ((($image["userfile"]["type"] == "image/gif")
				|| ($image["userfile"]["type"] == "image/jpeg")
				|| ($image["userfile"]["type"] == "image/pjpeg")
				|| ($image["userfile"]["type"] == "image/bmp")
				|| ($image["userfile"]["type"] == "image/png")
				|| ($image["userfile"]["type"] == "image/png")))
		//&& ($image["userfile"]["size"] < 2000000))
 {
			if ($image["userfile"]["error"] > 0) {
				error_log("Return Code: " . $image["userfile"]["error"], 0);
			} else {
				//error_log("inside else", 0);

				//error_log()
				//echo "Upload: " . $image["userfile"]["name"] . "<br />";
				//echo "Type: " . $image["userfile"]["type"] . "<br />";
				//echo "Size: " . ($image["userfile"]["size"] / 1024) . " Kb<br />";
				//echo "Temp file: " . $image["userfile"]["tmp_name"] . "<br />";

				if (file_exists(
						$this->folder_name . "/" . $image["userfile"]["name"])) {
					error_log(
							$image["userfile"]["name"] . " already exists. ",
							0);

					//echo $image["userfile"]["name"] . " already exists. ";
				} else {
					//error_log("inside else2", 0);

					if (move_uploaded_file($image['userfile']['tmp_name'],
							$target_path)) {
						error_log(
								"The file "
										. basename($_FILES['userfile']['name'])
										. " has been uploaded", 0);
					} else {
						error_log(
								"There was an error uploading the file, please try again!",
								0);
					}
				}
			}
		} else {
			//echo "Invalid file";
			$error = "Invalid file type";
			error_log("invalid file", 0);
			return $error;
		}

		$this->image_url_original = $filename;
		return 0;

	}

	public function resizeImage($width, $height) {
		//echo "<br/>filename: " . $this->image_url_original;
		$filename = $this->folder_name . $this->image_url_original;
		list($orig_width, $orig_height) = getimagesize($filename);

		$fp = fopen($filename, 'rb');
		if ($fp == false)
			return "Image '$filename' not found!";
		$buf = '';
		//To open the image as effortlessly as possible, we will be using GD�s function imagecreatefromstring (which takes a binary string of data as input), instead of imagecreatefromjpeg, imagecreatefrompng or imagecreatefromgif
		while (!feof($fp)) {
			$buf .= fgets($fp, 4096);
		}

		//error_log($buf, 0);

		// create image and assign it to our variable
		$image = imagecreatefromstring($buf);

		//save resized image
		$thumb_filename = $this->folder_name . "thumb/"
				. $this->image_url_original;
		//echo "<br/> thumb filename: " . $thumb_filename;
		//imagejpeg($image,$thumb_filename,100);

		// modify the image to create a thumbnail
		$thumb = imagecreatetruecolor($width, $height);
		imagecopyresampled($thumb, $image, 0, 0, 0, 0, $width, $height,
				$orig_width, $orig_height);
		//echo "<br/>gdf:" . $thumb;

		//imagejpeg($tmp,$filename,100);

		imagejpeg($thumb, $thumb_filename, 100);

		// Set the content type header - in this case image/jpeg
		//header('Content-Type: image/jpeg');

		// Skip the filename parameter using NULL, then set the quality to 75%
		//imagejpeg($thumb, NULL, 75);

		imagedestroy($thumb);
		imagedestroy($image);
		$this->image_url_thumb = "thumb/" . $this->image_url_original;
		return 0;
		//imagedestroy($tmp1);
	}

	public function save_item() {
		error_log("inside save item", 0);

		$dbconn = connect();

		$sql = "INSERT INTO `item` (`title`, `used`, `description`, `image_url_original`, `image_url_thumb`, `category_id`, `created_at`, `updated_at`, `user_id`) 
		VALUES ('$this->title', '$this->used', '$this->description', '$this->image_url_original', '$this->image_url_thumb', '$this->category_id', now(), now(), '$this->user_id');";
		$error = $dbconn->query($sql);
		//error_log("this: " . $error, 0);
		$this->id = $dbconn->insert_id;
		/*$query = $dbconn->prepare($sql);
		 $query->execute(array(':title'=>$this->title,
		        ':used'=>$this->used,
		        ':description'=>$this->description,
		        ':image_url_original'=>$this->image_url_original));*/

		//$result = $dbconn->query($sql);

		$dbconn->close();
		return $this->id;
	}

	public function getImageUrl() {
		return $this->image_url_original;
	}

	//change item row to associative array
	public function getItemArray($id) {
		$result = $this->getItemWithId($id);
		$itemArray = $result->fetch_assoc();
		//$itemArray = array('id'=>$this->id, 'title'=>$this->title, 'description'=>$this->description, 'image_name'=>$this->image_url_original);
		return $itemArray;

	}

	public function getItemWithId($id) {
		$url = "http://" . $_SERVER['HTTP_HOST'] . "/clutterly_staging/images/";
		$dbconn = connect();
		$sql = "select id, title, used, description, created_at, updated_at, concat('$url', image_url_original) as image_url_original, concat('$url', image_url_thumb) as image_url_thumb, category_id, user_id, decided from item where id = "
				. $id;
		$result = $dbconn->query($sql);
		$dbconn->close();
		return $result;
	}

	public function getRecentItemsWithCategory($num = 5, $category_id,
			$last_item_id, $last_created_at) {
		//echo "fdasfa: " . $last_item_id . "<br/>";
		$url = "http://" . $_SERVER['HTTP_HOST'] . "/clutterly_staging/images/";
		//echo "num: $num, category_id: $category_id <br/>";
		$dbconn = connect();
		$sql = "select id, title, used, description, created_at, updated_at, concat('$url', image_url_original) as image_url_original, concat('$url', image_url_thumb) as image_url_thumb, category_id, user_id, decided
				from item
				where category_id = $category_id and decided != 1 ";
		if ($last_item_id != 0) {
			$result = $dbconn
					->query(
							"select created_at from item where id = $last_item_id");
			$row = mysqli_fetch_assoc($result);
			//$created_at = strtotime($row["created_at"]);
			$sql .= " and created_at < '" . $row['created_at'] . "' ";
		} else if($last_created_at != "") {
			$sql .= " and created_at < '" . $last_created_at . "' ";
		}
		$sql .= " order by created_at desc limit $num ";
		//echo $sql;
		//echo "query: " . $sql;
		$result = $dbconn->query($sql);
		$dbconn->close();
		//echo "fffffffffffffffff: " . $result;
		return $result;
	}

	public function getRecentItems($num = 5, $last_item_id, $last_created_at) {
		//echo $last_created_at . "<br/>";
		$url = "http://" . $_SERVER['HTTP_HOST'] . "/clutterly_staging/images/";
		//echo "num: $num, category_id: $category_id <br/>";
		$dbconn = connect();
		$sql = "select id, title, used, description, created_at, updated_at, concat('$url', image_url_original) as image_url_original, concat('$url', image_url_thumb) as image_url_thumb, category_id, user_id, decided from item ";
		$sql .= " where decided != 1 ";
		if ($last_item_id != 0) {
			$result = $dbconn
					->query(
							"select created_at from item where id = $last_item_id");
			$row = mysqli_fetch_assoc($result);
			//$created_at = strtotime($row["created_at"]);
			$sql .= " and created_at < '" . $row['created_at'] . "' ";
		} else if($last_created_at != "") {
			$sql .= " and created_at < '" . $last_created_at . "' ";
		}
		$sql .= "order by created_at desc limit $num";
		//echo "query1: " . $sql;
		$result = $dbconn->query($sql);
		$dbconn->close();
		//echo "fffffffffffffffff: " . $result;
		return $result;
	}

	public function getRemainingItemCount($created_at, $category_id = 0) {
		$sql = "select count(*) count from item where created_at < '$created_at' and decided != 1  ";
		if ($category_id != 0)
			$sql .= " and category_id = $category_id ";
		//echo $sql;
		$dbconn = connect();
		$result = $dbconn->query($sql);
		$dbconn->close();
		$row = mysqli_fetch_assoc($result);
		return $row["count"];
	}

	public function getAllItemsWithCategory($category_id) {
		$url = "http://" . $_SERVER['HTTP_HOST'] . "/clutterly_staging/images/";
		//echo "num: $num, category_id: $category_id <br/>";
		$dbconn = connect();
		$sql = "select id, title, used, description, created_at, updated_at, concat('$url', image_url_original) as image_url_original, concat('$url', image_url_thumb) as image_url_thumb, category_id, user_id, decided from item where category_id = $category_id order by created_at desc";
		//echo "query: " . $sql;
		$result = $dbconn->query($sql);
		$dbconn->close();
		//echo "fffffffffffffffff: " . $result;
		return $result;
	}

	public function getAllItemsWithUser($user_id, $old_items, $last_item_id = 0) {
		$url = "http://" . $_SERVER['HTTP_HOST'] . "/clutterly_staging/images/";
		$dbconn = connect();
		$sql = "select id, title, used, description, created_at, updated_at, concat('$url', image_url_original) as image_url_original, concat('$url', image_url_thumb) as image_url_thumb, category_id, user_id, decided 
				from item 
				where user_id = $user_id ";
		/*if($last_item_id != 0) {
		    $result = $dbconn->query("select created_at from item where id = $last_item_id");
		    $row = mysqli_fetch_assoc($result);
		    //$created_at = strtotime($row["created_at"]);
		    $sql .= " and created_at < '" . $row['created_at'] . "' ";
		}*/
		$sql .= " order by created_at desc ";

		//echo $sql; 	
		$result = $dbconn->query($sql);
		$dbconn->close();
		return $result;
	}

	public function markDecided($id) {
		$dbconn = connect();
		$sql = "update item set decided = true where id = $id";
		$result = $dbconn->query($sql);
		return $result;
	}

	public function deleteItem($id) {
		$dbconn = connect();

		$sql = "select id, title, used, description, created_at, updated_at, image_url_original, image_url_thumb, category_id, user_id, decided from item where id = "
				. $id;
		$result = $dbconn->query($sql);
		$item = $result->fetch_assoc();

		//close if file is open
		$myFile = $this->folder_name . $item["image_url_original"];
		$fh = fopen($myFile, 'w');
		//error_log("file: " . $fh, 0);
		if ($fh) {
			fclose($fh);
			//delete original file
			unlink($myFile);

			//close if file is open
			$myFile = $this->folder_name . $item["image_url_thumb"];
			$fh = fopen($myFile, 'w');
			if ($fh) {
				fclose($fh);
				//delete thumbnail
				unlink($myFile);

				$sql = "delete from item where id = $id";
				//error_log($sql, 0);
				$result = $dbconn->query($sql);
				$dbconn->close();
				return result;
			} else
				return false;
		} else
			return false;
	}

	public function updateItem($id, $title, $description, $used, $category_id,
			$decided) {
		$dbconn = connect();

		$sql = "update item set title='$title', used=$used, description='$description', category_id='$category_id', decided=$decided where id = "
				. $id;
		$result = $dbconn->query($sql);
		return $sql;
	}

}

?>