<?php
include "item.php";
include "../utils.php";

$user_id = get_userid($_GET["access_token"]);
//$user_id = "100000263201445";
$item =  new Item();
//$last_item_id = isset($_GET["last_item_id"]) ? intval($_GET["last_item_id"]) : 0;

if ($user_id !== 0) {
	$result = $item->getAllItemsWithUser($user_id, $_GET["items"]); //, $last_item_id);//, $_GET["last_item_id"]);
	if (mysqli_num_rows($result)) {
		while ($item = $result->fetch_assoc()) {
			$items[] = array('item' => $item);
		}
	}
	header('Content-type: application/json');
	echo json_encode(array('items' => $items));
} else {
	RestUtils::sendResponse(400, "Invalid Access Token");
}
