<?php
include "item.php";
include "../classes/RestUtils.php";
include "../utils.php";

$method = $_SERVER['REQUEST_METHOD'];
if ($method == "DELETE") {
	parse_str(file_get_contents('php://input'), $params);
	$GLOBALS["_{$method}"] = $params;
	// Add these request vars into _REQUEST, mimicing default behavior, PUT/DELETE will override existing COOKIE/GET vars
	$_REQUEST = $params + $_REQUEST;
	//error_log("inside save: " . $GLOBALS["_{$method}"][id], 0);

	$item = new Item();
	$item_id = intval($GLOBALS["_{$method}"][id]);
	
	$user_id = get_userid($GLOBALS["_{$method}"][access_token]);
	if ($user_id !== 0) {
		$item_result = $item->getItemArray($item_id);
		if (isset($item_result["id"])) {
			if ($user_id === $item_result["user_id"]) {
				$result = $item->deleteItem($item_id);
				if ($result) {
					RestUtils::sendResponse(200, json_encode(array("item" => array("id" => $item_id))));
				} else {
					RestUtils::sendResponse(400, "could not delete");
				}
			} else {
				RestUtils::sendResponse(400, "Item does not belong to user.");
			}
		} else {
			RestUtils::sendResponse(400, "Item does not exist.");
		}
	} else {
		RestUtils::sendResponse(400, "Invalid Access Token");
	}
}

?>