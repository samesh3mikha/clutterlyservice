<?php
include "item.php";
include "../classes/RestUtils.php";

//error_log("indise add item", 0);
//include "../stdlib.php";
//error_log("title: " . $_POST["title"], 0);
//error_log("desc: " . $_POST["description"], 0);
//error_log("used: " .  $_POST["used"], 0);
//error_log("category_id: " . $_POST["category_id"], 0);
//error_log("filename: " . $_FILES["userfile"]["name"], 0);
//error_log("tmp_filename: " . $_FILES["userfile"]["tmp_name"], 0);
//error_log("file contents: ". var_dump($_FILES));

$item = new Item($_POST["title"], $_POST["description"], $_POST["used"], $_POST["category_id"], $_POST["access_token"]);
//echo $_FILES['userfile']['name'];
//echo var_dump($_FILES['userfile']);
$error = $item->saveImage($_FILES);
//echo "url" . $item->getImageUrl();

if($error===0) {
	$error=$item->resizeImage(100,100);
	if($error===0) {
		$id = $item->save_item();
		if($id != 0) {
			RestUtils::sendResponse(200, json_encode(array('item'=>$item->getItemArray($id))), 'application/json');
		} else {
			RestUtils::sendResponse(400, "could not save");
		}
	} else {
		RestUtils::sendResponse(400, $error);
	}
} else {
	RestUtils::sendResponse(400, $error);
}

?>