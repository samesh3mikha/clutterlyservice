<?php
include "item.php";
include "../classes/RestUtils.php";
include "../utils.php";

//error_log("inside update", 0);

$method = $_SERVER['REQUEST_METHOD'];
if ($method == "PUT") {
	parse_str(file_get_contents('php://input'), $params);
	$GLOBALS["_{$method}"] = $params;
	// Add these request vars into _REQUEST, mimicing default behavior, PUT/DELETE will override existing COOKIE/GET vars
	$_REQUEST = $params + $_REQUEST;
	$item_id = intval($GLOBALS["_{$method}"][id]);
	//error_log("item_id:" . $item_id, 0);
	$item = new Item();

	//RestUtils::sendResponse(200, $item->markDecided($item_id));
	$user_id = get_userid($GLOBALS["_{$method}"][access_token]);
	if ($user_id !== 0) {
		$item_result = $item->getItemArray($item_id);
		if (isset($item_result["id"])) {
			if ($user_id === $item_result["user_id"]) {
				$title = isset($GLOBALS["_{$method}"][title]) ? $GLOBALS["_{$method}"][title] : $item_result["title"];
				$description = isset($GLOBALS["_{$method}"][description]) ? $GLOBALS["_{$method}"][description] : $item_result["description"];
				$used = isset($GLOBALS["_{$method}"][used]) ? $GLOBALS["_{$method}"][used] : $item_result["used"];
				$category_id = isset($GLOBALS["_{$method}"][category_id]) ? intval($GLOBALS["_{$method}"][category_id]) : intval($item_result["category_id"]);
				$decided = isset($GLOBALS["_{$method}"][decided]) ? $GLOBALS["_{$method}"][decided] : $item_result["decided"];
				//$r = $item->updateItem($item_id, $title, $description, $used, $category_id, $decided);
				//echo $r;
				if ($item->updateItem($item_id, $title, $description, $used, $category_id, $decided)) {
					//error_log("giving response", 0);
					RestUtils::sendResponse(200,
					json_encode(
					array(
					'item' => $item
					->getItemArray($item_id))),
					'application/json');
				} else {
					RestUtils::sendResponse(400, "Item does not exist.");
				}
			} else {
				RestUtils::sendResponse(400, "Item does not belong to user.");
			}
		} else {
			RestUtils::sendResponse(400, "Item does not exist.");
		}
	} else {
		RestUtils::sendResponse(400, "Invalid Access Token");
	}

}
