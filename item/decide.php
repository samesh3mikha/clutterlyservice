<?php
include "item.php";
include "../classes/RestUtils.php";
include "../utils.php";

$method = $_SERVER['REQUEST_METHOD'];
if ($method == "PUT") {
	parse_str(file_get_contents('php://input'), $params);
	$GLOBALS["_{$method}"] = $params;
	// Add these request vars into _REQUEST, mimicing default behavior, PUT/DELETE will override existing COOKIE/GET vars
	$_REQUEST = $params + $_REQUEST;
	$item_id = intval($GLOBALS["_{$method}"][id]);
	$item = new Item();

	//RestUtils::sendResponse(200, $item->markDecided($item_id));
	$user_id = get_userid($GLOBALS["_{$method}"][access_token]);
	if ($user_id !== 0) {
		$item_result = $item->getItemArray($item_id);
		if (isset($item_result["id"])) {
			if ($user_id === $item_result["user_id"]) {
				if ($item->markDecided($item_id)) {
					RestUtils::sendResponse(200,
							json_encode(
									array(
											'item' => $item
													->getItemArray($item_id))),
							'application/json');
				} else {
					RestUtils::sendResponse(400, "Item does not exist.");
				}
			} else {
				RestUtils::sendResponse(400, "Item does not belong to user.");
			}
		} else {
			RestUtils::sendResponse(400, "Item does not exist.");
		}
	} else {
		RestUtils::sendResponse(400, "Invalid Access Token");
	}

}
