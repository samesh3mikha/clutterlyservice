<?php
include "item.php";
$item = new Item();
$result = $item->getAllItemsWithCategory($_GET['category_id']);
$items = array();
if(mysqli_num_rows($result)) {
	while($item = $result->fetch_assoc()) {
		$items[] = array('item'=>$item);
	}
}

header('Content-type: application/json');
echo json_encode(array('items'=>$items));
?>