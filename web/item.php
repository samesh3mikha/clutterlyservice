<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <?php 
  include_once "../item/item.php";
  $item_id = intval($_GET['item_id']);
  $item = new Item();
  $item_data = $item->getItemArray($item_id);
  	echo "<meta name='description' content='" . $item_data['description'] . "' />";
	echo "<meta name='title' content='Check this item on the clutterly webpage.'/>";
	echo "<meta name='image' content='" . $item_data['image_url_original'] . "'/>";
	echo "<meta name='url' content='http://" . $_SERVER['HTTP_HOST'] . "/clutterly_staging/web/item.php?item_id=$item_id'/>";
	//echo "<meta name='name' content='View this item on the clutterly webpage.'/>";
	echo "<meta name='site_name' content='Clutterly'/>";
	echo "<meta name='type' content='website'/>";
	//echo "<meta name='message' content='just a funny fasd'>";
	echo "<meta property='og:description' content='" . $item_data['description'] . "' />";
	echo "<meta property='og:title' content='Check this item on the clutterly webpage.'/>";
	//echo "<meta property='og:title' content='" . $item_data['title'] . "'/>";
	echo "<meta property='og:image' content='" . $item_data['image_url_original'] . "'/>";
	echo "<meta property='og:url' content='http://" . $_SERVER['HTTP_HOST'] . "/clutterly_staging/web/item.php?item_id=$item_id'/>";
	//echo "<meta property='og:name' content='View this item on the clutterly webpage.'/>";
	echo "<meta property='og:site_name' content='Clutterly'/>";
	echo "<meta property='og:type' content='website'/>";
	//echo "<meta property='og:message' content='just a funny msg'/>";
	//add later as website is not given as bajratechnologies
	//echo "<meta property='fb:app_id' content='311809422214746' />";
?>
  <meta http-equiv="Content-Style-Type" content="text/css"/>
  
  <title></title>
  <style type="text/css">
  <!-- @import url(css/style.css); -->
  </style>
</head>
<body>
<div id="header-container">
    <div id="header">
        <img src="images/logo.png"/>
        <h1>clutterly</h1>
    </div>
</div>
<div id="content">
<?php
include_once "../item/item.php";
include_once '../comment/comment.php';
include_once "../utils.php";
function xTimeAgo ($oldTime, $timeType) {
		$newTime = get_current_db_time();
        $timeCalc = strtotime($newTime) - strtotime($oldTime); 
        if ($timeType == "x") {
            if ($timeCalc >= 60) {
                $timeType = "m";
            }
            if ($timeCalc >= (60*60)) {
                $timeType = "h";
            }
            if ($timeCalc >= (60*60*24)) {
                $timeType = "d";
            }
        }
        if ($timeType == "s") {
            $timeCalc .= " second(s) ago";
        }
        if ($timeType == "m") {
            $timeCalc = round($timeCalc/60) . " minute(s) ago";
        }        
        if ($timeType == "h") {
            $timeCalc = round($timeCalc/60/60) . " hour(s) ago";
        }
        if ($timeType == "d") {
            $timeCalc = round($timeCalc/60/60/24) . " day(s) ago";
        }        
        return $timeCalc;
}

$item_id = intval($_GET['item_id']);
$item = new Item();
$item_data = $item->getItemArray($item_id);
if(!empty($item_data)) { 
	$comment = new Comment();
	$comment_result = $comment->get_comment_from_item($item_id);
?>

    <div id="user"><img src="https://graph.facebook.com/<?php echo $item_data["user_id"] ?>/picture"/><h1><?php echo get_username($item_data["user_id"])?></h1><h2><?php echo "Posted " . xTimeAgo($item_data["updated_at"], "x") ?></h2></div>
    <div id="item">
        <img src="<?php echo $item_data["image_url_original"]?>"/>
        <h1><?php echo $item_data["title"]?></h1>
        <p><?php echo $item_data["description"]?></p>
            
    </div>
    <div id="advice">
        <h1>Advice</h1>
        <?php 
        if(mysqli_num_rows($comment_result)) {
			while($comment = $comment_result->fetch_assoc()) {
        		echo "<div id='comment'><img src='https://graph.facebook.com/" . $comment['user_id']. "/picture'><p>" . $comment['comment_text'] . "</p></div>";
			}
		}
		?>
    </div>    
<?php } else {?>
	<div id="user"><h1><?php echo "Item with that id was not found." ?></h1></div>
<?php } ?>
</div>

</body>
</html>

