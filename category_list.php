<?php

include "database_connection.php";

$format = "json";
$dbconn = connect();

$sql = 'SELECT * from category;';
$result = $dbconn->query($sql);

$categories = array();
if(mysqli_num_rows($result)) {
	while($category = $result->fetch_assoc()) {
		$categories[] = array('category'=>$category);
	}
}

$returnArray["categories"] = $categories;

$sql = "select max(updated_at) last_updated from category";
$result = $dbconn->query($sql);
//echo implode(',', $result->fetch_assoc());
$returnArray = array_merge($result->fetch_assoc(), $returnArray);


if($format == 'json') {
	header('Content-type: application/json');
	echo json_encode($returnArray);
}
else {	
	header('Content-type: text/xml');
	echo '<categories>';
	foreach($categories as $index => $category) {
		if(is_array($category)) {
			foreach($category as $key => $value) {
				echo '<',$key,'>';
				if(is_array($value)) {
					foreach($value as $tag => $val) {
						echo '<',$tag,'>',htmlentities($val),'</',$tag,'>';
					}
				}
				echo '</',$key,'>';
			}
		}
	}
	echo '</categories>';
}
$dbconn->close();
?>