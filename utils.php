<?php
include_once "database_connection.php";

function get_userid($access_token) {
	$graph_url = "https://graph.facebook.com/me?access_token=";
	$url = $graph_url . $access_token;
	$headers = get_headers($url);
	if (substr($headers[0], 9, 3) == '200') {
		$user = json_decode(file_get_contents($url));
		$user_id = $user->id;
	}
	if($user_id == "") {
		return 0;
	} else {
		return $user_id;
	}
}

function get_username($userid) {
	$url = "https://graph.facebook.com/" . $userid;
	$headers = get_headers($url);
	if (substr($headers[0], 9, 3) == '200') {
		$user = json_decode(file_get_contents($url));
		$username = $user->name;
	}
	if($username == "") {
		return 0;
	} else {
		return $username;
	}
}

function get_userimage($userid) {
	
}

//function to get current time in order to compare with inserted time in db since php gives utc or timezone time
function get_current_db_time() {
	$dbconn = connect();
	$result = $dbconn->query("select now() now from item");
	$value = $result->fetch_assoc();
	return $value['now'];		
}
